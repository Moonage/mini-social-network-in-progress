Template.postsList.helpers({
  posts: function(){
    return Post.find({}, {sort: {created: -1}});
  }
});

Template.postsList.events({
  'click .follow-link': function(event){
    event.preventDefault();

    Meteor.call('follow', this);
  }
});
